var fps            = 9999;
var loopGameFlag   = true;
var wins           =  0;
var gamesPlayed    =  0;
var holdPoint      = 177; //min value bot will average lose to.
var enemyHoldPoint = 170; //3710: if($scope.party.enemyParty > 170) {
var endPoint       = 210; //can't go over this!
var arraySize      = endPoint+1;
var canvasChoice   = -1; //-1 to graph all options but old, or select specific index
var scoreMod       = 5;
var distanceMod    = 5;
var canvasMod      = 30*distanceMod/scoreMod;
var tempWins       = 0;
var tempGames      = 0;

var DebatePlayer = function() {

    var debateOptions = [
    /*transport: (30, 90)*/
        new DebateOption(0, 30, 90),
    /*civilian:  (1, 110)*/
        new DebateOption(1, 1, 110),
    /*business:  (50, 150)*/
        new DebateOption(2, 50, 150),
    /*hold                */
        new DebateOption(3)
    ];
    this.debateOptions = debateOptions;
    this.getScore = function(option, myScore, enemyScore){
        return debateOptions[option].getScore(myScore, enemyScore);
    };

    var noHoldOption = [].push(
        debateOptions[0],
        debateOptions[1],
        debateOptions[2]);

    //Current GameData
    var moveList       = []; //list of moves made during the game
    var holding        = false;
    var enemyHolding   = false;
    var currentVotes   = 0;
    var enemyVotes     = 0;

    var gameInterval;

    var currentTurn    = true;

    var save = function() {

    };
    this.save = save;

    var load = function () {
        for(var i = 0; i < debateOptions.length; i++) {
            debateOptions[i].load();
        }
    };
    this.load = load;

    this.deleteSave = function() {
        for(var i = 0; i < debateOptions.length; i++) {
            debateOptions[i].deleteSave();
        }

        wins = 0;
        gamesPlayed = 0;
    };

    function startGame() {
        if(debugLogs){
            console.log("Starting Debate game!");
        }
        moveList       = [];
        holding        = false;
        enemyHolding   = false;
        currentVotes   = rand(1, 110);
        enemyVotes     = rand(1, 110);
        currentTurn    = true;

        gameInterval = setInterval(playGame, 1 / fps * 1000);
        updateHTML();

    }

    function playGame() {
        if(isPaused){
            return;
        }

        if(currentTurn) {
            playTurn();
        } else {
            playEnemyTurn();
        }
        currentTurn = !currentTurn;

        checkGame();//check if game is over

        if(debugLogs){
            console.log("currentTurn:  " + currentTurn );
            console.log("currentVotes: " + currentVotes);
            console.log("enemyVotes:   " + enemyVotes  );
        }
    }

    function checkGame() {
        //check for bust
        if(currentVotes > 210){
            return endGame(false);
        }
        if(enemyVotes > 210){
            return endGame(true);
        }

        if(enemyVotes > 170) {
            enemyHolding = true;
        }

        if(holding) {
            if(enemyHolding){
                var isWon  = (currentVotes >= enemyVotes);
                return endGame(isWon);
            }

            if(enemyVotes > currentVotes) {
                return endGame(false);
            }
        }
    }

    function endGame(isWon) {

        clearInterval(gameInterval);
        updateDebateOptions(isWon);

        if(isWon) {
            wins++;
            tempWins++;
        }
        gamesPlayed++;
        tempGames++;

        save();
        if(loopGameFlag) {
            startGame();
        }

        if((gamesPlayed%1000) === 0){
            drawCanvas();
        }


        updateHTML();
        return isWon;
    }

    function drawCanvas () {
        var width = 210;
        var height = 210;

        for (var i = 0; i < width+1; i++) {
            for (var j = 0; j < height+1; j++) {
                var red   = 0;
                var blue  = 0;
                var green = 0;
                if(canvasChoice < 0){
                    red   = Math.round((debateOptions[0].getScore(i, j)*canvasMod) + (255/2));
                    green = Math.round((debateOptions[1].getScore(i, j)*canvasMod) + (255/2));
                    blue  = Math.round((debateOptions[2].getScore(i, j)*canvasMod) + (255/2));
                }else{
                    var score = Math.round((debateOptions[canvasChoice].getScore(i, j)*255/3*2/30));

                    blue = score;
                    red = -score;
                }

                context.fillStyle = "rgb("+red+","+green+","+blue+")";
                context.globalAlpha = 1;

                context.fillRect(i*sM, j*sM, 1*sM, 1*sM);
            }
        }
    }
    this.drawCanvas = drawCanvas;

    function playTurn() {
        if(holding) {
            return;
        }
        var index = 0;
        if(currentVotes < enemyHolding){//hit
            index = getChoice(noHoldOption, currentVotes, enemyVotes);
        }else{//hit or hold
            index = getChoice(debateOptions, currentVotes, enemyVotes);
        }

        var choice = debateOptions[index];
        moveList.push({
            choice: index,
            votes: currentVotes,
            enemy: enemyVotes,
        });
        var hit = choice.hit();
        if(hit === "hold"){
            hold();
        } else{
            currentVotes += choice.hit();
        }
    }

    function playEnemyTurn() {
        if(enemyVotes < 170 || holding) {
            enemyHit();
        } else {
            enemyHolding = true;
        }
        if(enemyVotes < currentVotes && !enemyHolding) {

        }
    }

    //3685: $scope.party.enemyParty += generateDebate(110, 1);
    function enemyHit() {
        var enemyMinHit = 1;
        var enemyMaxHit = 110;
        var hit = rand(enemyMinHit, enemyMaxHit);
        enemyVotes += hit;
    }


    function hold() {
        holding = true;
    }


    function getChoice (debateOptions, currentVotes, enemyVotes) {
        var scores = [];
        var min = 0;
        for (var i = 0; i < debateOptions.length; i++) {
            var score = debateOptions[i].getScore(currentVotes, enemyVotes);
            if(score < min){
                min = score;
            }
            scores.push(score);
        }
        for (var i = 0; i < scores.length; i++) {
            scores[i] -= min;
        }
        return weightedRand(scores);
    }

    function weightedRand(weights){
        var total = weights.reduce(function(a, b) { return a + b; }, 0);
        var sum   = 0;
        var rand  = Math.random();
        var i;
        for (i = 0; i < weights.length; i++) {
            sum += weights[i]/total;
            if(sum > rand){
                return i;
            }
        }
        return Math.floor(Math.random() * (weights.length));
    }

    function updateDebateOptions(result) {

        if(result) {
            result = 1;
        } else {
            result = -1;
        }

        for(var i = 0; i < moveList.length; i++) {
            var option     = moveList[i].choice;
            var myScore    = moveList[i].votes;
            var enemyScore = moveList[i].enemy;
            debateOptions[option].update(myScore, enemyScore, result);
        }
    }

    this.startGame = startGame;
};


var DebateOption = function(id = -1, minHit = -1, maxHit = -1) {

    this.scoreList = null;
    this.getScore = function(currentVotes, enemyVotes) {
        var score       =  1;
        var maxDistance =  10;

        for(var i = currentVotes - maxDistance; i < currentVotes + maxDistance; i++) {
            if(i < 0){
                i = 0;
            }
            if(i >= this.scoreList.length){
                break;
            }

            for (var j = enemyVotes - maxDistance; j < enemyVotes + maxDistance; j++) {
                if(j < 0) {
                    j = 0;
                }
                if(j >= this.scoreList[i].length){
                    break;
                }

                var myDistance    = Math.abs(currentVotes-i);
                var enemyDistance = Math.abs(enemyVotes-j);
                var totalDistance = myDistance + enemyDistance;
                var baseScore = this.scoreList[i][j]*scoreMod;
                var val = baseScore / ((totalDistance)*distanceMod+1);

                score += val;
            }
        }
        recordScoreTotal += Math.abs(score);
        recordScoreCount++;

        return score;
    };


    this.update = function(indexX, indexY, increment) {
        this.scoreList[indexX][indexY] += increment;
    };

    this.hit = function() {
        if(minHit === -1 && maxHit === -1){
            return "hold";
        }
        return rand(minHit, maxHit);
    };

    this.save = function() {

    };

    this.load = function () {

    };

    this.deleteSave = function() {


        this.scoreList = new Array(arraySize);
        for (var i = 0; i < arraySize; i++) {
            this.scoreList[i] = new Array(arraySize).fill(0);
        }
    };
};

function updateHTML() {
    var winsText           = document.getElementById("wins");
    var gamesPlayedText    = document.getElementById("gamesPlayed");
    var winPercentText     = document.getElementById("winPercent");
    var tempWinPercentText = document.getElementById("tempWinPercent");
    var averageScore       = document.getElementById("averageScore");

    winsText.innerText           = wins;
    gamesPlayedText.innerText    = gamesPlayed;
    winPercentText.innerText     = (wins / gamesPlayed).toPrecision(5);
    tempWinPercentText.innerText = (tempWins / tempGames).toPrecision(5);
    averageScore.innerText       = (recordScoreTotal / recordScoreCount).toPrecision(5);
}

function rand(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

var canvas  = document.getElementById("myCanvas");
var context = canvas.getContext("2d");

var debugLogs = false;

var isPaused = false;
document.getElementById("pauseButton").addEventListener("click", function(){
    isPaused = !isPaused;

    if(isPaused){
        player.drawCanvas();
        document.getElementById("pauseButton").innerHTML = "resume";
    }else{
        document.getElementById("pauseButton").innerHTML = "pause";
    }
});

var recordScoreTotal = 0;
var recordScoreCount = 0;

var player = new DebatePlayer();
player.deleteSave();
//player.load();
setTimeout(player.startGame, 500);

var sM = 2;

var canvasWidth = canvasHeight = canvas.width = canvas.height = 210*sM;


canvas.addEventListener('click', function(e) {

    var x = (e.pageX - canvas.offsetLeft);
    var y = (e.pageY - canvas.offsetTop);


    var context = this.getContext('2d');
    var p = context.getImageData(x, y, 1, 1).data;

    var score = [];
    score[0]  = player.getScore(0, Math.floor(x/sM), Math.floor(y/sM));
    score[1]  = player.getScore(1, Math.floor(x/sM), Math.floor(y/sM));
    score[2]  = player.getScore(2, Math.floor(x/sM), Math.floor(y/sM));
    score[3]  = player.getScore(3, Math.floor(x/sM), Math.floor(y/sM));

    var vals  = [];
    vals[0]  = player.debateOptions[0].scoreList[Math.floor(x/sM)][Math.floor(y/sM)];
    vals[1]  = player.debateOptions[1].scoreList[Math.floor(x/sM)][Math.floor(y/sM)];
    vals[2]  = player.debateOptions[2].scoreList[Math.floor(x/sM)][Math.floor(y/sM)];
    vals[3]  = player.debateOptions[3].scoreList[Math.floor(x/sM)][Math.floor(y/sM)];

    var sum = score[0]+score[1]+score[2]+score[3];
    var scoresText   = "scores: " + score[0].toPrecision(3) +":"+ score[1].toPrecision(3) +":"+ score[2].toPrecision(3) + ":" + score[3].toPrecision(3);
    var valsText = "vals: " + vals[0].toPrecision(3) +":"+ vals[1].toPrecision(3) +":"+ vals[2].toPrecision(3) + ":" + vals[3].toPrecision(3);
    var coord = "myScore=" + Math.floor(x/sM)  + ", enemyScore=" + Math.floor(y/sM);
    var clickDiv = document.getElementById("clickDiv");
    clickDiv.innerHTML = (coord + "<br>" + scoresText + "<br>" + sum.toPrecision(3) +"<br>" + valsText);
});

function rgbToHex(r, g, b) {
    if (r > 255 || g > 255 || b > 255){
        throw "Invalid color component";
    }
    return ((r << 16) | (g << 8) | b).toString(16);
}